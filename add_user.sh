#!/bin/bash

# ./add_user.sh  admin/normal user_name  group_name


user_type=$1
date="$(date)"

if [ $user_type == "admin" ] 
 then
    admin_user_name=$2
    useradd -m $admin_user_name
    chown $admin_user_name:$admin_user_name /home/$admin_user_name
    echo "$date $admin_user_name as admin created successfully" >> logfiles.log
fi

if [ $user_type == "normal" ]
then
    normal_user_name=$2
    group_name=$3
    useradd -m $normal_user_name -g $group_name
    chown $normal_user_name /home/$normal_user_name
    chmod 775 /home/$normal_user_name
    echo "$date $normal_user_name created successfully in the group $group_name" >> logfiles.log
fi